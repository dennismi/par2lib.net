﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Par2Lib;
using System.Threading;
namespace par2client
{
    public partial class Form1 : Form
    {
        Par2Lib.Par2Parser parser = new Par2Lib.Par2Parser();
        public Form1()
        {
            InitializeComponent();
            Console.WriteLine(CRC32.Compute(ASCIIEncoding.ASCII.GetBytes("dennis")).ToString("x2"));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Par2 files (*.par)|";
            openFileDialog1.Multiselect = false;
            openFileDialog1.InitialDirectory = @"D:\download\finished\Real College Girls Lesbian Stories 2 (2009)";
            parser.fileChecked += new Par2Parser.CheckFileHandler(parser_fileChecked);
            parser.sliceChecked += new Par2Parser.CheckSliceHandler(parser_sliceChecked);
            parser.fileCheckStarted += new Par2Parser.CheckFileStartHandler(parser_fileCheckStarted);
            //parser.Load(@"D:\download\finished\Sanctuary (US) - 1x11 - Instinct\Sanctuary.S01E11.720p.BluRay.x264.NLsubs tvpinda.par2");
            if (openFileDialog1.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                parser.Load(openFileDialog1.FileName);
            }
            

            if (parser.IsLoaded)
            {
                parser.Parse();
            }
            dataGridView1.DataSource = parser.Files;
            
            //parser_lineParsed(string.Format("Found a total of {2} packets with {0} description packets and {1} verifications packets",parser.FileDescriptionsPackets.Count, parser.FileVerificationPackets.Count, parser.Packets.Count));
            //foreach (Par2File p in parser.Files)
            //{
            //    Console.WriteLine(p);
            //    parser_lineParsed(p.ToString());
            //}
            
            
        }
        delegate void UpdateProgressDel(int progress, int total);
        delegate void Finished(string fileID);
        private void UpdateProgress(int progress, int total)
        {
            if (total!=0)  progressBar1.Maximum = total+1;
            if (progress!=-1)progressBar1.Value = progress;
        }
        void parser_fileCheckStarted(int total)
        {
            int progress = 0;
            if (InvokeRequired)
                this.Invoke(new UpdateProgressDel(UpdateProgress),progress,total);
            else
                UpdateProgress(progress, total);
        }

        void parser_sliceChecked(int no_checked)
        {
            int total=0;
            if (InvokeRequired)
                this.Invoke(new UpdateProgressDel(UpdateProgress), no_checked, total);
            else
                UpdateProgress(no_checked, total);
        }

        void parser_fileChecked(string s, string fileID)
        {
            if (InvokeRequired)
            {
                this.Invoke(new UpdateProgressDel(UpdateProgress), -1, 0);
                this.Invoke(new Finished(FileFinished),fileID);
            }
            else
            {
                UpdateProgress(-1, 0);
                FileFinished(fileID);
            }
           
        }
        void FileFinished(string fileId)
        {
            //dataGridView1.Rows.
        }
        void parser_lineParsed(string s)
        {
            //listBox1.Items.Add(s);
        }

        private void checkFiles_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(parser.CheckFiles));
            t.Start();            
        }
    }
}

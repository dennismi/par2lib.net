﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Web.Configuration;
using System.Web.Security;

namespace Par2Lib
{
    public class Par2Parser
    {

        public static char[] MagicSequence = new char[] { 'P', 'A', 'R', '2', '\0', 'P', 'K', 'T' };
        public static char[] fileverificationpacket_type = new char[] { 'P', 'A', 'R', ' ', '2', '.', '0', '\0', 'I', 'F', 'S', 'C', '\0', '\0', '\0', '\0' };
        public static char[] filedescriptionpacket_type = new char[] { 'P', 'A', 'R', ' ', '2', '.', '0', '\0', 'F', 'i', 'l', 'e', 'D', 'e', 's', 'c' };
        public static char[] mainpacket_type = new char[] { 'P', 'A', 'R', ' ', '2', '.', '0', '\0', 'M', 'a', 'i', 'n', '\0', '\0', '\0', '\0' };
        public static char[] recoveryblockpacket_type = new char[] { 'P', 'A', 'R', ' ', '2', '.', '0', '\0', 'R', 'e', 'c', 'v', 'S', 'l', 'i', 'c' };
        public static char[] creatorpacket_type = new char[] { 'P', 'A', 'R', ' ', '2', '.', '0', '\0', 'C', 'r', 'e', 'a', 't', 'o', 'r', '\0' };
        public bool IsLoaded { get { return par2file.Length != 0; } }
        public string Magic_Sequence { get { return ConvertTypeToString(MagicSequence); } }
        public string fileverificationpacket { get { return ConvertTypeToString(fileverificationpacket_type); } }
        public List<Packet> FileVerificationPackets { get { return packets.Where(f => f.PacketType == fileverificationpacket).ToList(); } }
        public string filedescriptionpacket { get { return ConvertTypeToString(filedescriptionpacket_type); } }
        public List<Packet> FileDescriptionsPackets { get { return packets.Where(f => f.PacketType == filedescriptionpacket).ToList(); } }
        public string mainpacket { get { return ConvertTypeToString(mainpacket_type); } }
        public string creatorpacket { get { return ConvertTypeToString(creatorpacket_type); } }
        protected internal static string ConvertTypeToString(char[] type)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in type)
            {
                if (c == '\0') sb.Append(" ");
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }
        public Par2Parser()
        {

        }
        byte[] par2file;
        string filename;
        public void Load(string filename)
        {
            this.filename = filename;
            //_par2file = new StreamReader(filename,System.Text.Encoding.Default,true).ReadToEnd();
            par2file = File.ReadAllBytes(filename);

            //StringBuilder sb = new StringBuilder();
            //foreach (byte b in par2file)
            //{
            //    sb.Append(Convert.ToChar(b));   
            //}
            //_par2file = sb.ToString();
        }


        public delegate void lineParsedHandler(string s);
        public delegate void CheckSliceHandler(int no_checked);
        public delegate void CheckFileHandler(string s,string fileId);
        public delegate void CheckFileStartHandler(int total);
        public event lineParsedHandler lineParsed;
        public event CheckSliceHandler sliceChecked;
        public event CheckFileHandler fileChecked;
        public event CheckFileStartHandler fileCheckStarted;
        List<byte[]> segments = new List<byte[]>();
        List<Packet> packets = new List<Packet>();
        public List<Packet> Packets { get { return packets; } }
        public List<Par2File> Files { get; private set; }
        public void Parse()
        {
            byte[] pkt = new byte[0];
            Packet pack = new Packet(pkt);
            int magicInt = 0;
            for (int i = 0; i < par2file.Length; i++)
            {
                //if (magicInt != MagicSequence.Length)
                //    System.Console.WriteLine(par2file[i] + "||" + Convert.ToByte(MagicSequence[magicInt]));

                if (magicInt != MagicSequence.Length && par2file[i] == MagicSequence[magicInt])
                {//it follows the magicsequence, then we need to skip until we come to some data:)

                    magicInt++;
                    continue;
                }

                //ok, now the magicsequence have now been seen, now some info follows and then the packet
                //first the entire length of the packet which means we can read pktlength+magicsequence.length to the get entire packet.
                byte[] len = new byte[8];
                Array.Copy(par2file,i,len,0,len.Length);
                uint pktlength = FindLength(len);//par2file[i], par2file[i + 1], par2file[i + 2], par2file[i + 3], par2file[i + 4], par2file[i + 5], par2file[i + 6], par2file[i + 7]);
                if (pktlength % 4 != 0) Debugger.Break();
                pkt = new byte[pktlength];
                Array.Copy(par2file, i - magicInt, pkt, 0, pktlength);
                if (pkt.Length == 0) continue;
                pack = CreatePacketFromByteArray(pkt);
                i += Convert.ToInt32(pktlength) - magicInt - 1;
                magicInt = 0;
                if (pack == null) continue;
                pack.PacketLength = Convert.ToInt32(pktlength);
                packets.Add(pack);
            }
            Files = new List<Par2File>();
            foreach (FileDescriptionPacket p in FileDescriptionsPackets)
            {
                uint ss = ((MainPacket)Packets.First(F => F is MainPacket && F.FileSetID == p.FileSetID)).SliceSize;
                Par2File p2f = new Par2File(p, (FileVerificationPacket)FileVerificationPackets.First(f => ((FileVerificationPacket)f).FileID == p.FileID), Path.GetDirectoryName(filename), ss);
               
                Files.Add(p2f);
            }
            //Console.WriteLine("Could parse {0} packets.", packets.Where(f=>f.PacketType==this.filedescriptionpacket).ToList().Count);
        }

        void p2f_fileCheckStarted(int total)
        {
            if (fileCheckStarted != null) fileCheckStarted(total);
        }

        void p2f_sliceChecked(int no_checked)
        {
            if (sliceChecked != null)                sliceChecked(no_checked);
        }

        void p2f_fileChecked(string s,string fileId)
        {
            if (fileChecked!=null)                fileChecked(s,fileId);    
        }

        private Packet CreatePacketFromByteArray(byte[] pkt)
        {
            int start = 8 + 8 + 16 + 16 + 8;
            char startC = Convert.ToChar(pkt[start]);
            if (startC == filedescriptionpacket_type[8])
                return new FileDescriptionPacket(pkt);

            if (startC == creatorpacket_type[8])
                return new CreatorPacket(pkt);

            if (startC == mainpacket_type[8])
                return new MainPacket(pkt);

            if (startC == fileverificationpacket_type[8])
                return new FileVerificationPacket(pkt);

            if (startC == recoveryblockpacket_type[8])
                return new RecoveryBlockPacket(pkt);

            return null;
        }
        internal static uint FindLength(params byte[] p)
        {
            //if (!BitConverter.IsLittleEndian) Array.Reverse(p);

            return BitConverter.ToUInt32(p, 0);
            //if (p.Length == 1) return p[0];
            //int retVal = p[0];
            //int shifter = 8;
            //for (int i = 1; i < p.Length; i++)
            //{
            //    retVal = (p[i] << i * shifter) | retVal;
            //}
            //return retVal;
        }
        public void CheckFiles()
        {
            Files.ForEach(a => {
                a.fileChecked += new Par2File.CheckFileHandler(p2f_fileChecked);
                a.sliceChecked += new Par2File.CheckSliceHandler(p2f_sliceChecked);
                a.fileCheckStarted += new Par2File.CheckFileStartHandler(p2f_fileCheckStarted);
                a.CheckFile();
                a.fileChecked -= new Par2File.CheckFileHandler(p2f_fileChecked);
                a.sliceChecked -= new Par2File.CheckSliceHandler(p2f_sliceChecked);
                a.fileCheckStarted -= new Par2File.CheckFileStartHandler(p2f_fileCheckStarted);
            });
        }
    }

    public class Par2File
    {

        public delegate void CheckSliceHandler(int no_checked);
        public delegate void CheckFileHandler(string s,string fileID);
        public delegate void CheckFileStartHandler(int total);
        public event CheckSliceHandler sliceChecked;
        public event CheckFileHandler fileChecked;
        public event CheckFileStartHandler fileCheckStarted;
        FileDescriptionPacket Description { get; set; }
        FileVerificationPacket Verification { get; set; }
        string mBasedir = "";
        uint sliceSize = 0;
        string mFullFileName = "";
        public string FileID { get; set; }
        public Par2File()
        { }

        public Par2File(FileDescriptionPacket _description, FileVerificationPacket _verification, string basedir, uint slicesize)
            : this()
        {
            Description = _description;
            Verification = _verification;
            Status = "Unknown";
            mBasedir = basedir;
            mFullFileName = Path.Combine(mBasedir, FileName);
            FileID = Description.FileID;
            if (!File.Exists(mFullFileName))
            {
                Status = "Missing";
            }
            sliceSize = slicesize;
        }

        public string FileName { get { return Description.FileName; } }
        public string Status { get; private set; }
        public string FileSize { get { return Description.FileLength.ToString(); } }
        public override string ToString()
        {
            return "Filename: " + FileName + " Number of hashes: " + Verification.Hashes.Count;
        }
        public void CheckFile()
        {
            if (Status == "Missing") return;
            int crccounter = 0;
            int good = 0, bad = 0;

            //foreach (var crc in Verification.Hashes.Keys)
            //{
            //    Console.WriteLine("{0}={1}",crc,Verification.Hashes[crc]);
            //}
            
            using (StreamReader s = new StreamReader(mFullFileName))
            {
                using (Stream reader = s.BaseStream)
                {
                    byte[] b = new byte[16 * 1024];
                    reader.Read(b, 0, b.Length);
                    if (Packet.md5hash(b) != Description.FileHash_16K) { Status = "16K Failed"; return; }
                    reader.Position = 0;
                    b = new byte[reader.Length];
                    reader.Read(b, 0, b.Length);
                    if (Packet.md5hash(b) == Description.FileHash) { Status = "Complete";  }
                    b = new byte[sliceSize];
                    reader.Position = 0;
                    int slices = Convert.ToInt32(reader.Length / sliceSize);
                    if (fileCheckStarted != null) fileCheckStarted(slices);
                    CRC32 crc32 = new CRC32();
                    string sliceHash;
                    uint crc; ;
                    while (crccounter <= slices)
                    {
                        Console.WriteLine(crccounter);
                        Console.WriteLine(reader.Position);
                        b = new byte[sliceSize];
                        reader.Read(b, 0, b.Length);
                        Console.WriteLine(reader.Position);
                        Console.WriteLine("----");
                        crc32 = new CRC32();
                        sliceHash = Packet.md5hash(b);
                        crc = CRC32.Compute(b);

                        if (Verification.Hashes.ContainsKey(crc) && Verification.Hashes[crc].ToLower() == sliceHash.ToLower())
                        {
                            good++;
                        }
                        else bad++;
                        crccounter++;
                        sliceChecked(crccounter);
                        
                    }
                    b = null;
                }
            }
            
            if (bad != 0)
                Status = String.Format("Incomplete ({0}/{1} available)", good, Verification.Hashes.Count);

            fileChecked(Status,Description.FileID);

        }
    }


    public class Packet
    {
        protected int bodyStart = 64;
        public Packet(byte[] pkt)
        {
            if (pkt.Length == 0) return;
            byte[] work = new byte[16];
                Array.Copy(pkt,16,work,0,work.Length);
                PacketHash = work.ToHex();
                work = new byte[16];
                Array.Copy(pkt, 32, work, 0, work.Length);
                FileSetID = work.ToHex();
            //for (int i = 16; i <= 32; i++)
            //{
            //    packetHash = string.Concat(packetHash, Convert.ToChar(pkt[i]).ToString());

            //}
            //for (int i = 32; i <= 48; i++)
            //{
            //    recovID = string.Concat(recovID, Convert.ToChar(pkt[i]).ToString());

            //}
            

        }
        public int PacketLength { get; set; }
        public string PacketHash { get; set; }
        public string FileSetID { get; set; }
        public string PacketType { get; set; }
        public override string ToString()
        {
            return string.Format("PacketType: {0}, Length {1}, fileSetID:{2}", PacketType, PacketLength, FileSetID);
        }
        protected string CorrectedPacketType(char[] type)
        {
            return Par2Parser.ConvertTypeToString(type);
        }
        protected string ConvertByteArrToString(byte[] byteArr)
        {
            string retVal = "";
            for (int i = 0; i < byteArr.Length; i++) { if (byteArr[i] == '\0') continue;  retVal = string.Concat(retVal, Convert.ToChar(byteArr[i])); }
            return retVal;
        }
       
        //  public static string md5hash2(byte[] input)
        //{
        //    return md5hash(input);
        //    //System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        //    //byte[] bs = x.ComputeHash(input);
        //    //System.Text.StringBuilder s = new System.Text.StringBuilder();
        //    //foreach (byte b in bs)
        //    //{
        //    //    s.Append(b.ToString("x2").ToLower());
        //    //}
        //    //return s.ToString();

        //}
        public static string md5hash(byte[] input)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.ASCII.GetBytes(new string(Encoding.ASCII.GetChars(input))); 
            bs = x.ComputeHash(input);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }
        //public static string md5hash(char[] input)
        //{
        //    System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        //    byte[] bs = System.Text.Encoding.ASCII.GetBytes(new string(input));
        //    bs = x.ComputeHash(bs);
        //    System.Text.StringBuilder s = new System.Text.StringBuilder();
        //    foreach (byte b in bs)
        //    {
        //        s.Append(b.ToString("x2").ToLower());
        //    }
        //    return s.ToString();
        //}
    }

    public class FileDescriptionPacket : Packet
    {

        public FileDescriptionPacket(byte[] pkt)
            : base(pkt)
        {
            if (pkt.Length == 0) return;
            PacketType = CorrectedPacketType(Par2Parser.filedescriptionpacket_type);
            int workLength = 16;

            #region FileID
            byte[] work = new byte[workLength];
            Array.Copy(pkt, bodyStart, work, 0, workLength);
            FileID = work.ToHex();
            bodyStart += workLength;
            #endregion
            
            #region full hash
            work = new byte[workLength];
            Array.Copy(pkt, bodyStart, work, 0, workLength);
            FileHash = work.ToHex();
            bodyStart += workLength;
            #endregion
            
            #region hash 16k
            work = new byte[workLength];
            Array.Copy(pkt, bodyStart, work, 0, workLength);
            FileHash_16K = work.ToHex();
            bodyStart += workLength;
            #endregion
            
            #region File length
            workLength = 8;
             work = new byte[workLength];
            Array.Copy(pkt, bodyStart, work, 0, workLength);
            FileLength = Par2Parser.FindLength(work);
            #endregion
            
            #region FileName
            bodyStart += workLength;
            workLength = pkt.Length - bodyStart;
            work = new byte[workLength];
            Array.Copy(pkt, bodyStart, work, 0, workLength);
            FileName = ConvertByteArrToString(work);
            #endregion
        }

        public string FileID { get; set; }
        public string FileHash { get; set; }
        public string FileHash_16K { get; set; }
        public uint FileLength { get; set; }
        public string FileName { get; set; }

        public override string ToString()
        {
            return base.ToString() + ", FileID: " + FileID + ", FileName: " + FileName + ", FileLength: " + FileLength.ToString();
        }

    }

    public class CreatorPacket : Packet
    {
        string Creator { get; set; }
        public CreatorPacket(byte[] pkt)
            : base(pkt)
        {
            if (pkt.Length == 0) return;
            PacketType = CorrectedPacketType(Par2Parser.creatorpacket_type);
            int workLength = pkt.Length - bodyStart;
            if (workLength % 4 != 0) Debugger.Break();
            
            for (int i = 0; i < workLength; i++)
            {
                Creator = string.Concat(Creator, Convert.ToChar(pkt[i + bodyStart]));
            }

        }
        public override string ToString()
        {
            return base.ToString() + ", Creator: " + Creator;
        }
    }

    public class MainPacket : Packet
    {
        public uint SliceSize { get; set; }
        public long NumberOfFiles { get; set; }
        public MainPacket(byte[] pkt)
            : base(pkt)
        {
            if (pkt.Length == 0) return;
            PacketType = CorrectedPacketType(Par2Parser.mainpacket_type);
            byte[] work = new byte[8];
            Array.Copy(pkt, bodyStart, work, 0, 8);
            bodyStart += work.Length;
            SliceSize = Par2Parser.FindLength(work);
            work = new byte[4];
            Array.Copy(pkt, bodyStart, work, 0, 4);
            NumberOfFiles = Par2Parser.FindLength(work);
        }
        public override string ToString()
        {
            return string.Concat(base.ToString(), string.Format(" SliceSize: {0}, NumberOfFiles: {1}", SliceSize, NumberOfFiles));
        }
    }

    public class FileVerificationPacket : Packet
    {
        public string FileID { get; set; }
        public Dictionary<uint,string> Hashes { get; set; }
        public FileVerificationPacket(byte[] pkt)
            : base(pkt)
        {
            Hashes = new Dictionary<uint, string>();
            PacketType = CorrectedPacketType(Par2Parser.fileverificationpacket_type);
            int workLength = 16;
            byte[] work = new byte[workLength];
            Array.Copy(pkt, bodyStart, work, 0, workLength);
            FileID = work.ToHex();
            //for (int i = 0; i < workLength; i++)
            //    FileID = string.Concat(FileID, Convert.ToChar(pkt[i + bodyStart]));
            bodyStart += workLength;
            workLength = 20;
            work = new byte[workLength];

            while (bodyStart < pkt.Length)
            {
                //read 20 bytes into a new array
                Array.Copy(pkt, bodyStart, work, 0, workLength);
                //read the hash and save it
                byte[] hash_b = new byte[16];
                string hash = "";
                Array.Copy(work, 0, hash_b, 0, hash_b.Length);
                hash = hash_b.ToHex();
                //read crc and save it 
                byte[] crc_b = new byte[4];
                uint crc = 0;
                Array.Copy(work, hash_b.Length, crc_b, 0, crc_b.Length);
                crc = Par2Parser.FindLength(crc_b);
                //add to hash set
                Hashes.Add(crc,hash);
                bodyStart += workLength;
            }
        }
        public override string ToString()
        {
            return base.ToString() + ", FileID: " + FileID;
        }
    }

    public class RecoveryBlockPacket : Packet
    {
        public RecoveryBlockPacket(byte[] pkt)
            : base(pkt)
        {
            PacketType = CorrectedPacketType(Par2Parser.recoveryblockpacket_type);
        }
    }

}
public static class ArrayExtentions
{
    public static string ToHex(this byte[] bytes, bool upperCase=false)
    {
        StringBuilder result = new StringBuilder(bytes.Length * 2);

        for (int i = 0; i < bytes.Length; i++)
            result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

        return result.ToString();
    }
}